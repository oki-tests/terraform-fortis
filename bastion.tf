module "sg_bastion" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "bastion-sg"
  description = "Security group for bastion host"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp"]
  egress_rules        = ["all-all"]

  depends_on = [module.vpc]
}

module "ec2_bastion" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name  = "bastion"
  count = 1

  ami                    = local.ami
  instance_type          = local.instance_type
  key_name               = local.key_name
  vpc_security_group_ids = ["${module.sg_bastion.security_group_id}"]

  subnet_id  = module.vpc.public_subnets[0]
  depends_on = [module.vpc, module.sg_bastion]
}
