module "sg_frontend" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "frontend-sg"
  description = "Security group for frontend app"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["10.0.1.0/24"]
  ingress_rules       = ["http-8080-tcp", "ssh-tcp"]
  egress_rules        = ["all-all"]

  depends_on = [module.vpc]
}

module "ec2_frontend" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name  = "frontend"
  count = 1

  ami                    = local.ami
  instance_type          = local.instance_type
  key_name               = local.key_name
  vpc_security_group_ids = ["${module.sg_frontend.security_group_id}"]

  subnet_id  = module.vpc.private_subnets[0]
  depends_on = [module.vpc, module.sg_frontend]
}

module "sg_frontend_elb" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "frontend-elb-sg"
  description = "Security group for frontend ELB"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp"]
  egress_with_cidr_blocks = [
    {
      rule        = "http-8080-tcp"
      cidr_blocks = "10.0.128.0/24"
    }
  ]

  depends_on = [module.vpc]
}

module "elb_http" {
  source = "terraform-aws-modules/elb/aws"

  name = "frontend-lb"

  subnets         = ["${module.vpc.public_subnets[0]}"]
  security_groups = ["${module.sg_frontend_elb.security_group_id}"]
  internal        = false

  listener = [
    {
      instance_port     = 8080
      instance_protocol = "TCP"
      lb_port           = 443
      lb_protocol       = "TCP"
    }
  ]

  health_check = {
    target              = "TCP:8080"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  // ELB attachments
  number_of_instances = 1
  instances           = ["${module.ec2_frontend[0].id}"]

  depends_on = [module.ec2_frontend, module.sg_frontend_elb]
}
