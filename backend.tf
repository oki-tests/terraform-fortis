terraform {
  backend "s3" {
    encrypt        = "true"
    bucket         = "fortis-challenge-tf-state"
    region         = "us-east-1"
    key            = "fortis-challenge/terraform.tfstate"
  }
}
