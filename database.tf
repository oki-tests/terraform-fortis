module "sg_database" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "database-sg"
  description = "Security group for database"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["10.0.128.0/24"]
  ingress_rules       = ["mysql-tcp"]
  egress_rules        = ["all-all"]

  depends_on = [module.vpc]
}

module "ec2_database" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name  = "database"
  count = 1

  ami                    = local.ami
  instance_type          = local.instance_type
  key_name               = local.key_name
  vpc_security_group_ids = ["${module.sg_database.security_group_id}"]

  subnet_id  = module.vpc.private_subnets[1]
  depends_on = [module.vpc, module.sg_database]
}


# I've decided to deploy an EC2 as a database instead of RDS due to limitations using a single AZ (it's required a minimum of 2 AZs even using multiaz=false)

# module "db" {
#   source = "terraform-aws-modules/rds/aws"

#   identifier = "fortisdb"

#   engine            = "mysql"
#   engine_version    = "5.7"
#   instance_class    = "db.t3.micro"
#   allocated_storage = 5

#   db_name  = "fortis"
#   username = "user"
#   port     = "3306"

#   iam_database_authentication_enabled = true

#   vpc_security_group_ids = ["${module.sg_database.security_group_id}"]

#   # DB subnet group
#   create_db_subnet_group = true
#   subnet_ids             = ["${module.vpc.private_subnets[1]}"]

#   # DB parameter group
#   family = "mysql5.7"

#   # DB option group
#   major_engine_version = "5.7"

#   # Database Deletion Protection
#   deletion_protection = false

#   depends_on = [module.vpc, module.sg_database]
# }
