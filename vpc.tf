module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  # The rest of arguments are omitted for brevity

  name = "fortis-vpc"

  cidr = "10.0.0.0/16"

  azs = ["us-east-1a"]

  public_subnets          = ["10.0.1.0/24"]
  map_public_ip_on_launch = true

  private_subnets = ["10.0.128.0/24", "10.0.129.0/24"]

  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
}
