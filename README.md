# terraform-fortis

This repo contains all the terraform code used to deploy the challenge.

## Install dependencies

- `brew install tfenv`
- `tfenv install`
- `tfenv use 1.8.2`

## Prerequisites

These are some prerequisites before applying Terraform:

- You must have an AWS credential configured on your machine.
- You need to create a SSH Key pair and replace the name in `locals.tf`
- You need to configure an S3 bucket to save the terraform state, and replace the bucket name in `providers.tf`

## How to apply this repo

You need to run:

```
terraform init
terraform plan
terraform apply
```

## Well-known limitations

- I've decided to deploy an EC2 as a database instead of RDS due to limitations using a single AZ (it's required a minimum of 2 AZs even using multiaz=false). It's possible to deploy a second AZ but this would require a new Nat Gateway + Internet Gateway in the other AZ to reduce inter-az network traffic costs.

- I've decided to hardcode the CIDR but it's possible to use the AWS IPAM to manage it.

- I've decided to not use AWS SSM to simplify the challenge, but it would be a better option than using SSH keys.

- I've decided to keep TCP on the ELB to reduce extra effort of configuring SSL, certificates, and DNS

- I've decided not use a dynamo table to protect and lock the terraform state to reduce extra effort of configuring DynamoDB tables.
